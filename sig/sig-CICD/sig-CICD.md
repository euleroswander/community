openEuler CICD Special Interest Group (SIG)
English | [简体中文](./sig-CICD_cn.md)

The openEuler CICD SIG will maintain CI/CD tools and services that can be used
by other openEuler projects.

## SIG Mission and Scope

### Mission

- maintain CI/CD packages
- support their usage in openEuler community

### Scope

- develop crystal-ci upstream project
- create and update RPM packages
- manage the documents, meetings and mailing lists

### Deliverables

- Source and RPM

### Repositories and description managed by this SIG

Repository for crystal-ci: https://gitee.com/openeuler/crystal-ci

## Basic Information

### Project Introduction
    https://gitee.com/openeuler/community/tree/master/sig/sig-CICD/

### Maintainers
- wu_fengguang
- jimmy_hero
- walkingwalk

### Committers
- wu_fengguang
- jimmy_hero
- walkingwalk

### Mailing list
- crystal-ci@openeuler.org

### External Contact
- wu_fengguang
